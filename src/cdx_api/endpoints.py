"""The api server endpoints, which will be serviced.
    Uses the flask package
"""
#Note: take care PYTHONPATH is set correctly; then remove these pylint switches
from flask import render_template               # pylint: disable=W0611
from cdx_api import app                         # pylint: disable=E0401
from models import predict_model, train_model   # pylint: disable=E0401,E0611

@app.route('/')
def index():
    """The standard entry point"""
    #for future use
    return 'Set up complete'

@app.route('/train')
def train():
    """Access point to train the model for the current data-set"""
    #train model
    return train_model.train()

@app.route('/predict')
def predict():
    """Access point to get a prediction"""
    return predict_model.predict()

@app.route('/status')
def status():
    """Access point to retrieve the current status"""
    return 'status'
